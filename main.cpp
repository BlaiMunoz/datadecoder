#include <iostream>
#include <assert.h>

#include "dataRecorder.h"

int main()
{
	std::array<int, arrayLength> myByteArray{0xA5,0x11,0x06,0x05,0x60,0x70};
	std::array<std::string, 2> decodedData = printInfo(myByteArray);
	assert(decodedData[0] == "DME");
	assert(decodedData[1].compare("70600506"));

	std::array<int, arrayLength> HEXByteArray{0xB8,0x20,0x12,0x34,0x56,0x78};
	std::array<std::string, 2> decodedDataHEX = printInfo(HEXByteArray);
	assert(decodedDataHEX[0] == "HEX");
	assert(decodedDataHEX[1].compare("78563412"));

	std::array<int, arrayLength> MAPByteArray{0x30,0x34,0xAB,0xCD,0x00,0xFF};
	std::array<std::string, 2> decodedDataMAP = printInfo(MAPByteArray);
	assert(decodedDataMAP[0] == "MAP");
	assert(decodedDataMAP[1].compare("FF00CDAB"));
	return 0;
}
