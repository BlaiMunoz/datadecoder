#include <iostream>
#include <iomanip>

#include "dataRecorder.h"

std::string getSerialFromArray(const std::array<int, arrayLength> arrayWithdata);
std::string getManufacturerFromArray(const std::array<int, arrayLength> arrayWithdata);

std::array<std::string, 2> printInfo(const std::array<int, arrayLength> myByteArray)
{
	std::array<std::string, 2> decodedData;

	decodedData[0] = getManufacturerFromArray(myByteArray);
	decodedData[1] = getSerialFromArray(myByteArray);

	std::cout << "Manufacturer: " << decodedData[0] << '\n';
	std::cout << "The Serial Number is: " << decodedData[1] << '\n';

	return decodedData;
}

std::string getSerialFromArray(const std::array<int, arrayLength> arrayWithdata)
{
	std::string serialDecoded{'\0'};
	for(int i= (arrayLength - 1); i >= arrayManufacturerBytes ; i--)
	{
		std::stringstream s;
		s << std::setfill('0') << std::setw(2) << std::hex << arrayWithdata[i];
		serialDecoded.append(s.str());
	}
	return serialDecoded;
}

std::string getManufacturerFromArray(const std::array<int, arrayLength> arrayWithdata)
{
	int number = (arrayWithdata[1] << 8) + arrayWithdata[0];
	char letters[3] = {'\0'};
	letters[0] = (number >> 10) + 64;
	letters[1] = ((number >> 5) & 0X1F) + 64;
	letters[2] = (number & 0X1F) + 64;
	std::string manufacturerString(letters);
	return manufacturerString;
}
