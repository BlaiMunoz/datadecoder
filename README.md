# DataDecoder

This repo contains the code to required to pass the Embedded Systems Engineer position test.

## The task
The code prints a human readable manufacturer and serial number from a 6 byte array.

### Installation guide
To install the necessary packages use the following command
````
sh install.sh
````

### Building guide
To build the program use the following command
command
````
sh buildProject.sh
````

### The Demo
To launch the demo use the following command
command
````
./DataRecorder
````

## Test method
I have used 3 byte arrays to check the code works. The first one has been given by the client and
the other 2 have been calculated adhoc. The byte arrays are:

Byte array	| Manufacturer  | Serial Number
------------- | ------------- | -------------
A51106056070  | DME		   | 70600506
B82012345678  | HEX		   | 78563412
3034ABCD00FF  | MAP		   | FF00CDAB

The expected output and the actual outpout have been compared.
