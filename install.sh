sudo apt-get install build-essential libssl-dev
cd /tmp
wget https://github.com/Kitware/CMake/releases/download/v3.16.3/cmake-3.16.3.tar.gz
tar -zxvf cmake-3.16.3.tar.gz
cd cmake-3.16.3
./bootstrap
make
sudo make install
cmake --version