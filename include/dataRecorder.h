#ifndef _DATA_RECORDER_H_
#define _DATA_RECORDER_H_
#include <array>

const int arrayLength = 6;
const int arrayManufacturerBytes = 2;

std::array<std::string, 2> printInfo(const std::array<int, arrayLength> myByteArray);

#endif /* _DATA_RECORDER_H_ */